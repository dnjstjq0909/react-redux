import { PAGENUM, PAGEVIEWN, SEARCH } from "./types";

export const search = (state) => { // search reducer 함수, state는 필터 옵션 값을 받고 리턴해준다.
  // console.log("search")
  return {
    type: SEARCH,
    state
  }
}

export const actionPageNum = (state) => {
  // console.log("pageNum")
  return {
    type: PAGENUM,
    state
  }
}

export const actionPageViewN = (state) => {
  // console.log("pageViewN")
  return {
    type: PAGEVIEWN,
    state
  }
}