import { legacy_createStore } from "redux";
import { combineReducers } from '@reduxjs/toolkit';
import filterReducer from './reducer/filterReducer';
import pageReducer from "./reducer/pageRecuer";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "persist",
  storage, // localStorage에 저장합니다.
  // filterReducer, pageReducer 2개 reducer를 localstorage에 저장합니다.
  whitelist: ["filterReducer", "pageReducer"]
  // blacklist -> 그것만 제외합니다
};

const reducer = combineReducers({
  filterReducer,
  pageReducer
});

// localStorage에 들어갈 Reducer 값들
export const localStorageReducer = persistReducer(persistConfig, reducer);

export const store = legacy_createStore(localStorageReducer);



// store;