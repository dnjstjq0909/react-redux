import { SEARCH } from "../types";

const localStorageInfo = JSON.parse(localStorage.getItem("persist:persist"));

const storageFilterOption = localStorageInfo ? JSON.parse(localStorageInfo.filterReducer) : {};

const initialFilterStatus = { // 필터옵션 초기 값 설정 (구조 설정)
  status: storageFilterOption?.status !== "total" ? storageFilterOption.status : "total",
  option: storageFilterOption?.option !== "name" ? storageFilterOption.option : "name",
  keyWord: storageFilterOption?.keyWord !== "" ? storageFilterOption.keyWord : ""
}

const filterReducer = (state = initialFilterStatus, action) => {
  switch (action.type) {
    case SEARCH:
      return action.state;
    default:
      return { ...state };
  }
}

export default filterReducer;