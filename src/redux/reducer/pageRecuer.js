import { PAGENUM, PAGEVIEWN } from "../types";

const localStorageInfo = JSON.parse(localStorage.getItem("persist:persist"));

const storageFilterOption = localStorageInfo ? JSON.parse(localStorageInfo.pageReducer) : {};

const initialPageStatus = { // 페이지옵션 초기 값 설정 (구조 설정)
  pageStatus: {
    pageNum: storageFilterOption?.pageStatus ? storageFilterOption.pageStatus.pageNum : "1",
    pageViewN: storageFilterOption?.pageStatus ? storageFilterOption.pageStatus.pageViewN : "10"
    // pageNum: "1",
    // pageViewN: "10"
  }
}

const pageReducer = (state = initialPageStatus, action) => {
  switch (action.type) {
    case PAGENUM: // 페이지 번호가 변하면 실행되는 리듀서
      return { ...state, pageStatus: { pageViewN: state.pageStatus.pageViewN, pageNum: action.state } };
    case PAGEVIEWN: // N개씩 보기가 변하면 실행되는 리듀서
      return { ...state, pageStatus: { pageViewN: action.state, pageNum: state.pageStatus.pageNum } };
    default:
      return { ...state };
  }
}

export default pageReducer;