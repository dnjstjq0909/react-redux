const onKeyUpEnter = (e, func) => {
  e.preventDefault();
  e.stopPropagation();
  switch (e.key) {
    case "Enter": {
      func(e);
    }
  }
}
export default onKeyUpEnter;