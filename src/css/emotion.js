import { jsx, css } from '@emotion/react'

export const flexBox = () => css`
  display: flex;
`;

export const margin = (top, right, bottom, left) => css`
  margin-top: ${top ? top : "0"};
  margin-right: ${right ? right : "0"};
  margin-bottom: ${bottom ? bottom : "0"};
  margin-left: ${left ? left : "0"};
`;

export const searchButton = () => css`
  background-color: #ccc;
  padding: 4px;
  font-size: 12px;
  color: #000;
  border: 1px solid #000;
  cursor: pointer;
`;

export const pageNumbtn = () => css`
  background-color: #ccc;
  padding: 4px;
  font-size: 12px;
  color: #000;
  border: 1px solid #000;
  cursor: pointer;
  margin: 10px;
`;

export const onPage = () => css`
  background-color: #000;
  font-size: 700;
  color: #fff;
`;