/** @jsxImportSource @emotion/react */
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { flexBox, margin, onPage, pageNumbtn, searchButton } from "../css/emotion";
import onKeyUpEnter from "../modules/onKeyUpEnter";
import { search, actionPageNum, actionPageViewN } from "../redux/action";

const Home = () => {
  // 로컬 스토리지에서 리듀서로 만든 값 가져오기
  const localStorageInfo = JSON.parse(localStorage.getItem("persist:persist"));

  // 로컬 스토리지에서 초기 값 체크 후 필터리듀셔 값 가져오기
  const storageFilterOption = JSON.parse(localStorageInfo && localStorageInfo.filterReducer);
  // 로컬 스토리지에서 초기 값 체크 후 페이지리듀서 값 가져오기
  const storagePageOption = JSON.parse(localStorageInfo && localStorageInfo.pageReducer);

  // 로컬 스토리지에 각각의 리듀서 값이 있으면 가져오고 없으면 초기 값 지정
  const [pageNum, setPageNum] = useState(storagePageOption ? storagePageOption?.pageStatus?.pageNum : "1");
  const [pageViewN, setPageViewN] = useState(storagePageOption ? storagePageOption?.pageStatus?.pageViewN : "10");

  const getPageNum = ["1", "2", "3", "4", "5"];

  const didMount1 = useRef();

  const filter = useSelector((state) => state); // Store에 있는 데이터의 변화 감지 및 업데이트
  const dispatch = useDispatch(); // Store에 있는 reducer를 실행하기 위해 dispatch

  //필터 옵션을 객체로 묶어서 저장, 초기값은 로컬 스토리지에 있는지 없는지 체크 후 가져오기
  const [filterOption, setFilterOption] = useState(storageFilterOption?.status ? storageFilterOption : {
    status: "total",
    option: "name",
    keyWord: ""
  });

  const onChangeStatus = (e) => { // 상태값 onChange함수
    setFilterOption((prevState) => { // 필터 옵션의 각각의 옵션마다 객체에 저장하는 로직
      return { ...prevState, status: e.target.id }
    })
  }

  const onChangeKeyWord = (e) => {
    setFilterOption((prevState) => {
      return { ...prevState, keyWord: e.target.value }
    })
  }

  const onChangeOption = (e) => {
    setFilterOption((prevState) => {
      return { ...prevState, option: e.target.value }
    })
  }

  const onChangePageViewN = (e) => {
    setPageViewN(e.target.value)
  }

  const onClickSearch = () => { // 검색 버튼 클릭 시 실행되는 함수
    dispatch(search(filterOption)) // 검색 시 dispatch를 통해 Store에 있는 Search Reducer실행

    // 검색 시 pageNum 초기화
    dispatch(actionPageNum("1"))
    setPageNum("1")
  }

  useEffect(() => {
    dispatch(actionPageNum(pageNum))
  }, [pageNum])

  useEffect(() => {
    dispatch(actionPageViewN(pageViewN))
  }, [pageViewN])

  useEffect(() => {
    if (didMount1.current) {

    } else {
      didMount1.current = true;
    }
  }, [filter])

  return (
    <>
      <div css={[flexBox, margin("50px", "50px", "50px", "50px")]}>
        상태 :
        <ul css={flexBox}>
          <li onClick={() => setFilterOption((prevState) => {
            return { ...prevState, status: "total" }
          })}>
            <input type="radio" id="total" checked={filterOption.status === "total"} onChange={(e) => onChangeStatus(e)} />
            <label htmlFor="total">전체</label>
          </li>
          <li onClick={() => setFilterOption((prevState) => {
            return { ...prevState, status: "admin" }
          })}>
            <input type="radio" id="admin" checked={filterOption.status === "admin"} onChange={(e) => onChangeStatus(e)} />
            <label htmlFor="admin">관리자</label>
          </li>
          <li onClick={() => setFilterOption((prevState) => {
            return { ...prevState, status: "manager" }
          })}>
            <input type="radio" id="manager" checked={filterOption.status === "manager"} onChange={(e) => onChangeStatus(e)} />
            <label htmlFor="manager">매니저</label>
          </li>
          <li onClick={() => setFilterOption((prevState) => {
            return { ...prevState, status: "wait" }
          })}>
            <input type="radio" id="wait" checked={filterOption.status === "wait"} onChange={(e) => onChangeStatus(e)} />
            <label htmlFor="wait">대기중</label>
          </li>
        </ul>
      </div>
      <div css={flexBox}>
        <div css={margin("0px", "0px", "0px", "30px")}>사용자 검색</div>
        <select css={margin("0px", "30px", "0px", "30px")} onChange={(e) => onChangeOption(e)} value={filterOption.option}>
          <option value="name">이름</option>
          <option value="email">이메일</option>
          <option value="auth">권한</option>
        </select>
        <input type="text" value={filterOption.keyWord} onChange={(e) => onChangeKeyWord(e)} onKeyUp={(e) => onKeyUpEnter(e, onClickSearch)} />
        <div css={[searchButton, margin("0px", "0px", "0px", "30px")]} onClick={onClickSearch}>검색</div>
      </div>
      <div>
        <select css={margin("30px", "30px", "0px", "30px")} onChange={(e) => onChangePageViewN(e)} value={pageViewN}>
          <option value="10">10개씩 보기</option>
          <option value="20">20개씩 보기</option>
          <option value="50">50개씩 보기</option>
        </select>
        <div css={flexBox}>
          {
            getPageNum.map((el, index) => {
              return (
                <div key={`${el}_${index}`} css={[pageNumbtn, pageNum === el && onPage]} id={`${el}`} onClick={(e) => setPageNum(e.target.id)}>{el}</div>
              )
            })
          }
        </div>
      </div>
    </>
  )
}

export default Home;